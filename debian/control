Source: igv
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Olivier Sallou <osallou@debian.org>,
           Pierre Gruet <pgt@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               default-jdk,
               gradle-debian-helper,
               javahelper
Build-Depends-Indep: inkscape,
                     libbatik-java,
                     libcommons-io-java,
                     libcommons-lang3-java,
                     libcommons-math3-java,
                     libdsiutils-java,
                     libfastutil-java,
                     libfest-assert-java,
                     libfest-util-java,
                     libfest-reflect-java,
                     libgoogle-gson-java,
                     libguava-java,
                     libhtsjdk-java,
                     libicb-utils-java,
                     libjaxp1.3-java,
                     libjide-oss-java,
                     libjsap-java,
                     liblog4j1.2-java,
                     liblog4j2-java,
                     libnb-absolutelayout-java,
                     libprotobuf-java,
                     libswing-layout-java,
                     libxml-commons-external-java,
                     openjfx,
                     junit4 <!nocheck>,
                     junit5 <!nocheck>,
                     libcommons-lang-java <!nocheck>
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/igv
Vcs-Git: https://salsa.debian.org/med-team/igv.git
Homepage: https://www.broadinstitute.org/igv/
Rules-Requires-Root: no

Package: igv
Architecture: all
Depends: ${misc:Depends},
         ${java:Depends},
         default-jre
Recommends: libmariadb-java
Description: Integrative Genomics Viewer
 The Integrative Genomics Viewer (IGV) is a high-performance viewer that
 efficiently handles large heterogeneous data sets, while providing a
 smooth and intuitive user experience at all levels of genome resolution.
 A key characteristic of IGV is its focus on the integrative nature of
 genomic studies, with support for both array-based and next-generation
 sequencing data, and the integration of clinical and phenotypic data.
 Although IGV is often used to view genomic data from public sources,
 its primary emphasis is to support researchers who wish to visualize and
 explore their own data sets or those from colleagues. To that end, IGV
 supports flexible loading of local and remote data sets, and is
 optimized to provide high-performance data visualization and exploration
 on standard desktop systems.
